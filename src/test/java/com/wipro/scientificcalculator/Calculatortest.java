package com.wipro.scientificcalculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class Calculatortest {
	 Trigcalc trigcalc = new Trigcalc();
     
	 @Test
	    public void testGetSinValue0Degree() {
	        double expected = 0.0;
	        double actual = trigcalc.getSinValue(0);
	        assertEquals(expected, actual, 0.001);
	    }

	    @Test
	    public void testGetSinValue30Degree() {
	        double expected = 0.5;
	        double actual = trigcalc.getSinValue(30);
	        assertEquals(expected, actual, 0.001);
	    }
	    @Test
	    public void testGetSinValue90Degree1() {
	        double expected = 1.0;
	        double actual = trigcalc.getSinValue(90);
	        assertEquals(expected, actual, 0.001);
	    }

	   

}
